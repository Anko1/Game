var cells = [];
var myScore = 0;

$(document).ready(function () {

    cells = Array.from($('.cssBlock'));
scoreLogic()
    // $(cells[0]).addClass('face')

    randomFace();
    console.log(cells);
});

function randomFace() {

    setInterval(function() {
        $('.cssBlock').removeClass('face');
        $('.cssBlock').off("click");

        var random = Math.floor(Math.random() * cells.length);

        var celRan = $(cells[random]);
        celRan.addClass('face');

        celRan.click(scoreLogic)

        }, 500);

}

function scoreLogic() {
    $('#score').text(myScore++)
}
